FROM ambakshi/perforce-git-fusion
MAINTAINER James Edwards-Jones

# Upgrade to at least git-fusion 2017.2
# in order to use configure_https_auth_centos.sh with CENTOS 7
RUN rpm -q helix-git-fusion | grep -qE '(2016|2017\.1)' && yum install -y helix-git-fusion

# Avoid script failing on broken use of sysctl
RUN sed -e 's/service httpd start/httpd -t/g' -i /opt/perforce/git-fusion/libexec/configure_https_auth_centos.sh

ENV P4CHARSET none
ENV GFUSER git

# Set LC_ALL and LANG as configure-git-fusion.sh didn't do so in 2017.1
RUN echo "export LC_ALL=en_US.utf8" >> "`getent passwd $GFUSER | cut -d ':' -f 6`/.bashrc"
RUN echo "export LANG=en_US.utf8" >> "`getent passwd $GFUSER | cut -d ':' -f 6`/.bashrc"

RUN /opt/perforce/git-fusion/libexec/configure_https_auth_centos.sh

EXPOSE 22 1666 443

ADD ./run-gf-https.sh /
CMD ["/run-gf-https.sh"]

