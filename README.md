Extends https://github.com/ambakshi/docker-perforce with support for https

## Docker Usage

```
docker build -t gitfusion-http .
docker volume create --name=gfp4depot
docker run --detach --publish 1666:1666 --publish 2222:22 --publish 1943:443 --env-file ./envfile --volume gfp4depot:/data --env NAME=gfp4depot gitfusion-http && docker ps
```

## Local usage

### Install p4 client
```
wget http://cdist2.perforce.com/perforce/r16.2/bin.macosx105x86_64/p4
chmod +x p4
mv p4 /usr/local/bin/
```

### Test config
```
cat envfile
export P4CONFIG=envfile
export P4CLIENT=demo-workspace
p4 info
```

### Make first perforce changes
```
p4 client myclient
mkdir depot && cd depot
echo 'TODO' > README.md
p4 submit -d 'First submit'
```

### Setup first gitfusion repo
```
p4 sync # -f
wget https://raw.githubusercontent.com/spearhead-ea/git-fusion/master/bin/p4gf_config.global.txt
mkdir -p .git-fusion/repos/myrepo
mv p4gf_config.global.txt .git-fusion/repos/myrepo/p4gf_config
echo "[master]\ngit-branch-name = master\nview = //depot/... ..." >> .git-fusion/repos/myrepo/p4gf_config
p4 add .git-fusion/repos/myrepo/p4gf_config
p4 submit -d 'Created myrepo gitfusion config'
```

### Setup SSH for gitfusion
```
mkdir -p .git-fusion/users/p4admin/keys/
cp  ~/.ssh/id_rsa.pub .git-fusion/users/p4admin/keys/`whoami`
p4 add .git-fusion/users/p4admin/keys/`whoami`
p4 submit -d "Added gitfusion key for p4admin"
docker exec -it p4git_gfperforce_1 bash
su - git -c '/opt/perforce/git-fusion/libexec/p4gf_auth_update_authorized_keys.py'

git clone ssh://git@localhost:2222/myrepo.git
```

### Allow push to ignore commit author
```
sed -i '' '/^ignore-author-permissions/s/ no/ yes/' .git-fusion/p4gf_config
```