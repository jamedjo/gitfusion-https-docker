#!/usr/bin/with-contenv bash
set -ex

# httpd -k start
# 
# source /opt/perforce/git-fusion/home/perforce-git-fusion/.bashrc
# 
# exec ./run.sh

export NAME="${NAME:-p4depot}"

bash -x /usr/local/bin/setup-perforce.sh
bash -x /usr/local/bin/setup-git-fusion.sh

cp /opt/perforce/git-fusion/libexec/p4gf_submit_trigger_wrapper.sh /usr/local/bin/
/opt/perforce/git-fusion/libexec/p4gf_submit_trigger.py --install "$P4PORT" "$P4USER" "$P4PASSWD"

#TODO: Isolate these lines, as httpd is to only new thing we wish to run
# 	   Perhaps using `exec ./run.sh`, but after setup-git-fusion.sh has set .bashrc
source /opt/perforce/git-fusion/home/perforce-git-fusion/.bashrc
httpd -k start

exec /usr/sbin/sshd -D -e
